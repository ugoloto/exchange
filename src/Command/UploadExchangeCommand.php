<?php

namespace App\Command;

use App\Entity\ExchangeRate;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class UploadExchangeCommand extends Command
{
    protected static $defaultName = 'UploadExchange';
    protected static $defaultDescription = 'Upload exchange rate from  https://www.ecb.europa.eu';
    /**
     * @var EntityManagerInterface
     */

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $rates = $this->getXmlRowsAsArray();
        $itemRepo = $this->entityManager->getRepository(ExchangeRate::class);
        foreach ($rates['Cube'] as $rate) {
            $date = $rate['@time'];
            foreach ($rate['Cube'] as $row) {
               if ($existingRate = $itemRepo->findOneBy(['currency'=>$row['@currency']])){
                   $this->updateRate($existingRate, $row['@rate'], $row['@currency'], $date);
                   continue;
               }
               $this->createRate($row['@rate'], $row['@currency'], $date);


            }
        }
        $this->entityManager->flush();
        $io = new SymfonyStyle($input, $output);

        $io->success('Rates was updated or added');

        return Command::SUCCESS;
    }

    public function getXmlRowsAsArray()
    {
        $inputfile = 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml';
        return (new Serializer([new ObjectNormalizer()], [new XmlEncoder()]))->decode(file_get_contents($inputfile), 'xml');
    }

    public function updateRate($existingRate, $rate, $currency, $date)
    {
        $existingRate->setValue($rate);
        $existingRate->setDate($date);
        $this->entityManager->persist($existingRate);
    }

    public function createRate($rate, $currency, $date)
    {
        $newRate = new ExchangeRate();
//        $newRate->setDate($date);
        $newRate->setValue($rate);
        $newRate->setCurrency($currency);
//        dd($newRate);
        $this->entityManager->persist($newRate);
    }
}

