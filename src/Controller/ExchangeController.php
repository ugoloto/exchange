<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Intl\Currencies;


class ExchangeController extends AbstractController
{
    /**
     * @Route("/exchange", name="app_exchange")
     */
    public function index(): Response
    {
        $url = 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml';
        

        $currencies = Currencies::getCurrencyCodes();
        return $this->render('exchange/index.html.twig', [
            'controller_name' => 'ExchangeController',
            'currencies' => $currencies,
        ]);
    }
}
